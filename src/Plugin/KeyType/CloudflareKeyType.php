<?php

namespace Drupal\key_cloudflare\Plugin\KeyType;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key for Cloudflare credentials.
 *
 * @KeyType(
 *   id = "cloudflare_key",
 *   label = @Translation("Cloudflare"),
 *   description = @Translation("Cloudflare credentials"),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "cloudflare"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "api_key" = {
 *         "label" = @Translation("API Key"),
 *         "required" = false,
 *       },
 *       "api_token" = {
 *         "label" = @Translation("API Token"),
 *         "required" = false,
 *       },
 *     }
 *   }
 * )
 */
class CloudflareKeyType extends AuthenticationMultivalueKeyType {

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return Json::encode($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!is_array($key_value) || empty($key_value)) {
      return;
    }
    if (empty($key_value['api_key']) && empty($key_value['api_token'])) {
      $form_state->setError($form, $this->t('API key or token must be populated.'));
    }
  }

}
