<?php

namespace Drupal\key_cloudflare\Plugin\KeyInput;

use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyInputBase;

/**
 * Defines a key input for Cloudflare.
 *
 * @KeyInput(
 *   id = "cloudflare",
 *   label = @Translation("Cloudflare credentials"),
 *   description = @Translation("A collection of fields to store Cloudflare credentials.")
 * )
 */
class CloudflareKeyInput extends KeyInputBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'api_token' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $key = $form_state->getFormObject()->getEntity();
    $definition = $key->getKeyType()->getPluginDefinition();
    $fields = $definition['multivalue']['fields'];
    foreach ($fields as $id => $field) {
      $form[$id] = [
        '#type' => 'textfield',
        '#title' => $field['label'],
        '#required' => $field['required'],
        '#maxlength' => 4096,
        '#default_value' => $config[$id],
        '#attributes' => ['autocomplete' => 'off'],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processSubmittedKeyValue(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    return [
      'submitted' => $values,
      'processed_submitted' => $values,
    ];
  }
}
