## Key Cloudflare

Key Cloudflare is an extension to the Key module.

This module provides a new key type for supporting authentication against the
Cloudflare service. This key will capture both the API token and API key. Both
are not required, but can be configured on the same key regardless.

### Requirements

Requires the Key module.

### Install

Install key_cloudflare using a standard method for installing a contributed Drupal
module, either by downloading the package or using composer with

`composer require drupal/key_cloudflare`

### Usage

Follow the [Key](https://drupal.org/project/key) documentation for getting and
using keys by adding a `key_select` element to your form in order to select your key.

You can apply a filter to select only cloudflare keys:

```php
$form['cloudflare_auth'] = [
  '#type' => 'key_select',
  '#title' => $this->t('Cloudflare Auth'),
  '#key_filters' => ['type' => 'cloudflare_key'],
];
```

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
